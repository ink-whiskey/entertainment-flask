<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
	
	<div class="warp">
	
		<p class="logo logo-top replace">Ink Whiskey</p>
		
		<div class="content">
			
			<div class="hero">
				<img src="../wp-content/themes/soon/img/cartridge-drunk_hunt.png" />
			</div>
			
			<h1>The Entertainment Flask</h1>
			
			<p>By Ink Whiskey</p>
		
		</div>
		
		<div class="photo photo-pour">
			<img src="../wp-content/themes/soon/img/photo-pour.jpg" />
		</div>
		
		<div class="content">
		
			<h1>Start a Collection,<br />Select From 5 Custom Labels</h1>
			
			<ul class="cartridges">
				<li><img src="../wp-content/themes/soon/img/cartridge-metal_beer.png" /><p>Metal Beer</p></li>
				<li><img src="../wp-content/themes/soon/img/cartridge-legend_of_drink.png" /><p>Legend of Drink</p></li>
				<li><img src="../wp-content/themes/soon/img/cartridge-drunk_hunt.png" /><p>Drunk Hunt</p></li>
				<li><img src="../wp-content/themes/soon/img/cartridge-castle_vodka.png" /><p>Castle Vodka</p></li>
				<li><img src="../wp-content/themes/soon/img/cartridge-bar_hop_bros.png" /><p>Bar-Hop Bros.</p></li>
			</ul>
			<div class="clear"></div>
			
			<h1>Store Coming Soon!</h1>
		
		</div>
		
		<div class="photo photo-pocket">
			<img src="../wp-content/themes/soon/img/photo-pocket.jpg" />
		</div>
		
		<div class="content">
		
			<a href="http://kck.st/1aggWpn" target="_blank">
		
				<h1>Give us a 1up!<br />Back the Flask on</h1>
				
				<p class="kickstarter replace">Kick Starter</p>
			
			</a>
		
		</div>
		
		<div class="photo photo-shelf">
			<img src="../wp-content/themes/soon/img/photo-shelf.jpg" />
		</div>
		
		<div class="content">
		
			<h1>Send All Inquiries to<br /><a href="mailto:info@inkwhiskey.com" target="_blank">info @ inkwhiskey.com</a></h1>
			
			<p class="logo logo-bottom replace">Ink Whiskey</p>
			
			<p>&copy; Copyright Ink Whiskey 2013</p>
		
		</div>
	
	</div>

<?php get_footer() ?>