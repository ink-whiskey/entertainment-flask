/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.1",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.1.268",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'glass',
            type:'image',
            rect:['818px','940px','266px','317px','auto','auto'],
            overflow:'visible',
            fill:["rgba(0,0,0,0)",im+"glass.jpg",'0px','0px'],
            userClass:"glass"
         },
         {
            id:'pour',
            type:'image',
            rect:['931px','630px','40px','450px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"pour.jpg",'0px','0px'],
            userClass:"pour"
         },
         {
            id:'grip',
            type:'rect',
            rect:['302px','145px','71px','76px','auto','auto'],
            borderRadius:["0px","0px","0px","0px 0px"],
            fill:["rgba(255,78,78,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            userClass:"grip"
         },
         {
            id:'cartridge-drunk_hunt',
            type:'image',
            rect:['461','386','446px','500px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cartridge-drunk_hunt.png",'0px','0px'],
            userClass:"cartridge"
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_grip}": [
            ["color", "background-color", 'rgba(115,115,115,1.00)'],
            ["style", "border-top-left-radius", [0,0], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "left", '164px'],
            ["style", "width", '100px'],
            ["style", "top", '150px'],
            ["style", "border-bottom-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '60px'],
            ["style", "border-top-right-radius", [0,0], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '650px'],
            ["style", "height", '3200px'],
            ["style", "overflow", 'hidden']
         ],
         "${_glass}": [
            ["style", "top", '1126px'],
            ["style", "overflow", 'visible'],
            ["style", "height", '317px'],
            ["style", "left", '276px'],
            ["style", "width", '266px']
         ],
         "${_pour}": [
            ["style", "top", '352px'],
            ["style", "left", '310px'],
            ["style", "height", '41px']
         ],
         "${_cartridge-drunk_hunt}": [
            ["style", "top", '139px'],
            ["style", "left", '96px'],
            ["transform", "rotateZ", '0deg']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2250,
         autoPlay: false,
         timeline: [
            { id: "eid123", tween: [ "style", "${_grip}", "border-bottom-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid98", tween: [ "style", "${_pour}", "left", '400px', { fromValue: '310px'}], position: 0, duration: 1500 },
            { id: "eid24", tween: [ "color", "${_grip}", "background-color", 'rgba(115,115,115,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(115,115,115,1.00)'}], position: 0, duration: 0 },
            { id: "eid96", tween: [ "style", "${_pour}", "height", '348px', { fromValue: '41px'}], position: 0, duration: 1500 },
            { id: "eid126", tween: [ "style", "${_pour}", "height", '410px', { fromValue: '348px'}], position: 1500, duration: 750 },
            { id: "eid103", tween: [ "style", "${_grip}", "left", '164px', { fromValue: '164px'}], position: 0, duration: 0 },
            { id: "eid46", tween: [ "style", "${_grip}", "top", '1860px', { fromValue: '150px'}], position: 0, duration: 2250 },
            { id: "eid101", tween: [ "style", "${_grip}", "height", '60px', { fromValue: '60px'}], position: 0, duration: 0 },
            { id: "eid61", tween: [ "style", "${_cartridge-drunk_hunt}", "top", '2216px', { fromValue: '139px'}], position: 0, duration: 2250 },
            { id: "eid52", tween: [ "style", "${_pour}", "top", '1602px', { fromValue: '352px'}], position: 0, duration: 1500 },
            { id: "eid94", tween: [ "style", "${_pour}", "top", '2638px', { fromValue: '1602px'}], position: 1500, duration: 750 },
            { id: "eid102", tween: [ "style", "${_grip}", "width", '100px', { fromValue: '100px'}], position: 0, duration: 0 },
            { id: "eid122", tween: [ "style", "${_grip}", "border-bottom-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid49", tween: [ "transform", "${_cartridge-drunk_hunt}", "rotateZ", '-180deg', { fromValue: '0deg'}], position: 500, duration: 1000 },
            { id: "eid90", tween: [ "style", "${_glass}", "top", '2793px', { fromValue: '1126px'}], position: 0, duration: 2250 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "z");
