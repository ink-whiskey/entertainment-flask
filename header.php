<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!DOCTYPE html>

<html>

<head>

	<meta charset="utf-8"/>		
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Portland, OR">
    <meta name="author" content="David Carroll">
    
    <link href="../wp-content/themes/soon/style.css" rel="stylesheet" />
    
    <link rel="shortcut icon" href="img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/icon-144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/icon-114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/icon-72.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/icon-57.png">
    
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Press+Start+2P' rel='stylesheet' type='text/css'>
	
	<title>The Ink Whiskey Entertainment Flask</title>
	
</head>

<body lang="en">